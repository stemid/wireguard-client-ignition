variable "connections" {
  type = list(object({
    endpoint = string
    name = string
    address = string
    allowedips = string
    privatekey = string
    publickey = string
    presharedkey = string
    persistentkeepalive = optional(number, 30)
  }))
}
