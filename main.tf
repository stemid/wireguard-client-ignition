data "ignition_file" "wireguard_nmconnection" {
  count = length(var.connections)
  path = "/etc/NetworkManager/system-connections/${var.connections[count.index].name}.nmconnection"
  content {
    content = templatefile("${path.module}/templates/wireguard.nmconnection", {
      conf = var.connections[count.index]
    })
  }
  mode = 384
}

data "ignition_config" "config" {
  files = [for f in data.ignition_file.wireguard_nmconnection : f.rendered]
}
