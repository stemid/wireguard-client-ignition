# Wireguard Client for Fedora CoreOS

This should work on any distro with NetworkManager, it's very basic and just creates a templated file under ``/etc/NetworkManager/system-connections/<connections.name>.nmconnection``.

Check ``vars.tf`` for exact structure but you set the name like this.

```hcl
wg_connections = [
    {
        name = "wg"
    }
]
```
